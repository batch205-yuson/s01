let students = [
	"John",
	"Jane",
	"Jeff",
	"Kevin",
	"David"
];

console.log(students);


class Dog {

	constructor(name,breed,age){

		this.name = name;
		this.breed = breed;
		this.age = age;

	}

}

let dog1 = new Dog("Bantay","corgi",3);

console.log(dog1);

let person1 = {

	name: "Saitama",
	heroName: "One Punch Man",
	age: 30

};


console.log(person1);


//What array method can we use to add an item at the end of an array?
students.push("Henry");
console.log(students);

//What array method can we use to add an item at the start of an array?
students.unshift("Jeremiah");
console.log(students);

students.unshift("Christine");
console.log(students);

//What array method does the ooposite of push?
students.pop();
console.log(students);

//What array method does the opposite of unshift?
students.shift();
console.log(students);

//What is the difference between splice() and slice()
//slice() Selects a part of an array, and returns the new array (non-mutator)
//splice() Adds/Removes elements from an array (mutator)


//What is another kind of array method?
//Iterator methods loops over the items of an array.

//forEach() - loops over items in an array and repeats a user-defined function.

//map() - - loops over items in an array and repeats a user-defined function AND returns a new array.

//every() - loops and checks if all items satisfies a givene condition and returns a boolean.

let arrNum = [15,25,50,20,10,11];

//check if every item in arrNum is divisible by 5:
let allDivisible;
arrNum.forEach(num => {
	if(num % 5 === 0){
		console.log(`${num} is divisible by 5.`)
	} else {
		allDivisible = false;
	}

	//However, can forEach() return data that will tell us IF all numbers/items in our arrNum array is divisible by 5?

});

console.log(allDivisible);

/*arrNum.pop();
arrNum.push(35);*/
let divisibleby5 = arrNum.every(num => {

	//Every is able to return data.
	//When the function inside every() is able to return true, the loop will continue until all items are able to return true. Else, If at least one item returns false, then the loop will stop there and every() will return false.

	console.log(num);
	return num % 5 === 0;

});

console.log(divisibleby5);

//some() - loops and checks if at least one item satisfies a given condition and returns a boolean. It will return true if at least ONE item satisfies the condition, else If no item returns true/satisfies the condition, some() will return false.

let someDivisibleBy5 = arrNum.some(num => {

	//some() will stop its loop once a function/item is able to return true.
	//Then, some() will return true else, if all items does not satisfy the condition or return true, some() will return false.

	console.log(num);
	return num % 5 === 0

});

console.log(someDivisibleBy5);

arrNum.push(35);
let someDivisibleBy6 = arrNum.some(num => {

	console.log(num);
	return num % 6 === 0;
});

console.log(someDivisibleBy6);


//QUIZ
	/*
		1. How do you create arrays in JS?
			- Using the array literal which is []
		2. How do you access the first character of an array?
			- To access the first character of the items in an array you need to use .map() to loop over every item and to return the first character you need .charAt(0).
		3. How do you access the last character of an array?
			- To access the last character of an array you also need to use .map() to loop over every items in an array. To return the last character you need to use .slice(-1).
			ex. 
			I used the students array.

			let lastLetter = students.map(function(students) {
				return students.slice(-1);
			});

			console.log(lastLetter);

		4. What array method searches for, and returns the index of a given value in an array?
			a. This method returns -1 if given value is not found in the array.
			-indexOf()	
		5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
			-forEach()
		6. What array method creates a new array with elements/ data returned from a user-defined function?
			-filter()
		7. What array method checks if all its elements satisfy a given condition?
			-every()
		8. What array method checks if at least one of its elements satisfies a given condition?
			-some()
		9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
			-False
		10. True or False: array.slice() copies elements from original array and returns a new array.
			-True
		11. An array is a primitive data type. True or False?
			-False
		12. What global JS object contains pre-defined methods and properties used for mathematical calculations?
			-Math
		13. True or False? The number of characters in a string can be determined using the .length property because strings are actually objects too.
			-False
		14. True or False? The methods of an Array are user-defined?
			-False
		15. True or False? We can use the .toString() on a boolean.
			-True
	*/

//FUNCTION CODING


		const addToEnd = (arr, element) => {
			if(typeof element !== "string"){
				return "error - can only add strings to an array";
			}
			arr.push(element);
			return arr;
		}


		const addToStart = (arr, element) => {
			if(arr.length === 0){
				return "error - can only add strings to an array";
			}
			arr.unshift(element);
			return arr;
		}



		const elementChecker = (arr, elementChecked) => {
			if (arr.length === 0) {
				return "error - passed in array is empty";
			} 

			return arr.some(element => element === elementChecked);
		} 

		const stringLengthSorter = (arr) => {
			if(arr.some(element => typeof element !== "string")) 
				return "error - all array elements must be strings";
			arr.sort((a,b) => {
				return a.length - b.length;
			})
			return arr;			
		}
	
